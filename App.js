import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import React from 'react';

import Home from "./Rotas/Home";
import Detalhe from "./Rotas/Detalhe";
import FichaCadastro from "./Rotas/FichaCadastro";
import Estudos from "./Rotas/Estudos";
import Categorias from "./Rotas/Categorias";

let navegador = createStackNavigator({
  Home: {screen: Home, navigationOptions: 'none'},
  Detalhes: {screen: Detalhe, navigationOptions: 'none'},
  Ficha: {screen: FichaCadastro, navigationOptions: 'none'},
  Estudos: {screen: Estudos, navigationOptions: 'none'},
  Categorias: {screen: Categorias, navigationOptions: 'none'},

});

let App = createAppContainer(navegador);

export default App;