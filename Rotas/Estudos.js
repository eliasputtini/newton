import React, {Component} from 'react';
import {
    View,StyleSheet,StatusBar, Keyboard, Alert, TouchableOpacity,Text,TextInput, Image,FlatList,SafeAreaView, 

} from "react-native";

import AsyncStorage from '@react-native-community/async-storage';


const styles = StyleSheet.create({
    list:{
        paddingVertical: 5,
        alignSelf:"center",
        backgroundColor: "#d4b626"
    },
    container :{
        backgroundColor: '#ebcc34',
        flex :1,
        justifyContent:'flex-start',
    },
  viewConfig :{
      backgroundColor: '#ebcc34',
      flex: 1,
      alignItems: 'center',
      marginHorizontal:50,
      alignSelf:"flex-end",
      justifyContent:"flex-end",
      marginBottom:50,
    
  },list:{
    paddingVertical: 5,
    alignSelf:"center",
    backgroundColor: "#d4b626"
},

  logoText :{
      color : 'black',
      fontFamily:'PatrickHandSC-Regular',
      fontSize: 100, 
      textAlign: 'center',
  },
  inputBox:{
    fontFamily:'Raleway-Medium',
    width:300,
    backgroundColor:'white',
    borderRadius: 25,
    paddingHorizontal:16,
    marginVertical:10,

  },
  button:{
      width:300,
      backgroundColor:'#d4b626',
      borderRadius: 25,
      marginVertical:10,
      paddingHorizontal:16,
      paddingVertical:16
  },
  buttonConfig:{
    width:50,
    height:50,
    backgroundColor:'#d4b626',
    borderRadius: 50,
    marginVertical:10,
    paddingHorizontal:16,
    paddingVertical:16,
    justifyContent:'space-evenly',
},
  buttonText:{
      fontFamily:'Raleway-Medium',
      fontSize:20,
      fontWeight:'500',
      color:'black',
      justifyContent:'center',
      textAlign:'center'

  },print :{
    backgroundColor: '#ebcc34',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  textStyle:{
    fontFamily:'Raleway-Medium',
    fontSize:25,
    fontWeight:'500',
    flexWrap: 'wrap'
}
})

class Home extends Component {
    constructor(props){
        super(props);      
        this.state = {nome:"", kids: []};
    }

  static navigationOptions = {
    header:null
  };

  async leia(){
    try{
        let x = await AsyncStorage.getItem('NEWTON_APP');

        if (x!= null){

            console.log("dados carregados:"+x);
            this.setState({kids:  JSON.parse(x)})

            Alert.alert("Sucesso","Dados carregados com sucesso"+ x );
        }
    }catch(e){
        alert(e);
        
    }
  }

  UNSAFE_componentWillMount(){
    this.leia()
 }

  renderizaItem(item){
 
    return (
            <View style={styles.print}>
                <Text style={styles.textStyle}>Questão: {item.kidName}</Text>
                <Text style={styles.textStyle}>Resposta: {item.kidGender}</Text>
            </View>
            
    )
}


  render() {
    
    return (
        
        <View style={styles.container}>
        <StatusBar backgroundColor="#ebcc34" barStyle="dark-content" />

        <Text style={[styles.logoText,{paddingTop:50}]}>NEWTON</Text> 
          
        <SafeAreaView>

                    <FlatList
                        style={styles.list}
                        data={this.state.kids}
                        renderItem={ ({item}) =>  this.renderizaItem(item)}
                        keyExtractor={({kidName}) => kidName}
                    />
                </SafeAreaView>


            

        </View>
      );
  }
}

export default Home;