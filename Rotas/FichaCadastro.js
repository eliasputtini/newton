import React, {Component} from 'react';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import {
    View,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Text
} from "react-native";
import { SafeAreaView } from 'react-navigation';


const styles = StyleSheet.create({

    
    container :{
        
        justifyContent:"flex-start",
        backgroundColor: '#ebcc34',
        flex: 1,
        alignItems: 'center',

    },
    logoText :{
        fontSize:18,
        color : 'white'
    },
    inputBox:{
        fontFamily:'Raleway-Medium',
        width:350,
        height:500,
        backgroundColor:'white',
        borderRadius: 25,
        paddingHorizontal:16,
        marginVertical:10,
    },
    button:{
        width:300,
        backgroundColor:'#d4b626',
        borderRadius: 25,
        marginVertical:10,
        paddingHorizontal:16,
        paddingVertical:16


    },buttonText:{
        fontFamily:'Raleway-Medium',
        fontSize:20,
        fontWeight:'500',
        color:'black',
        justifyContent:'center',
        textAlign:'center'

    },
    textStyle:{
        fontFamily:'Raleway-Medium',
        fontSize:25,
        fontWeight:'500',
        flexWrap: 'wrap'
    }

})

var radio_props = [
    {label: 'Certo', value: 0 },
    {label: 'Errado', value: 1 }
];




class FichaCadastro extends Component {
    constructor(props){
        super(props);
        this.mudouQuestao = this.mudouQuestao.bind(this);



        this.cadastra = this.cadastra.bind(this);
        this.state={nome: "", resposta: ""};
    }

    static navigationOptions = {
        header:null
    };

    checaVazio(){
        if(this.state.nome == '' || this.state.resposta == '' ){
            alert('Vazio!')
        }else{
            this.cadastra();
        }
    }

    mudouQuestao(txt){
        this.setState({nome: txt});
    }

    
    cadastra(){
        this.props.navigation.getParam("funcCadastra")({kidName: this.state.nome, kidAge: this.state.idade, kidGender: this.state.resposta});
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={[styles.textStyle,{marginTop:50}]}>Digite a Questão:</Text> 
                    

                <SafeAreaView style={styles.fundo}>
                    <TextInput style={styles.inputBox} 
                        placeholder={"Digite a questão aqui..."} 
                        multiline={true}
                        Type={"String"} 
                        id={'iQuestion'} 
                        onChangeText={this.mudouQuestao} 
                    />
                </SafeAreaView>
                

                <RadioForm Style={styles}
                           radio_props={radio_props}
                           initial={0}
                           formHorizontal={true}
                           selectedButtonColor={"black"}
                           animation={true}
                           onPress={(value) => {this.setState({value:value})}}
                />
               

              

                <TouchableOpacity style={styles.button} title={'confirmar'} onPress={() => {this.checaVazio()}}>
                    <Text style={styles.buttonText}> Confirmar</Text>
                </TouchableOpacity>

            </View>
        );
    }
}
export default FichaCadastro;