import React, {Component} from 'react';
import {View, Text, Button,StatusBar, Image,StyleSheet,TouchableOpacity} from 'react-native';


const styles = StyleSheet.create({
  container :{
      backgroundColor: '#ebcc34',
      flex: 1,
      alignItems: 'center',
      

  },
  viewConfig :{
      backgroundColor: '#ebcc34',
      flex: 1,
      alignItems: 'center',
      marginHorizontal:50,
      alignSelf:"flex-end",
      justifyContent:"flex-end",
      marginBottom:25,
    
  },

  logoText :{
      color : 'black',
      fontFamily:'PatrickHandSC-Regular',
      fontSize: 100, 
      textAlign: 'center',
  },
  inputBox:{
    fontFamily:'Raleway-Medium',
    width:300,
    backgroundColor:'white',
    borderRadius: 25,
    paddingHorizontal:16,
    marginVertical:10,

  },
  button:{
      width:300,
      backgroundColor:'#d4b626',
      borderRadius: 25,
      marginVertical:10,
      paddingHorizontal:16,
      paddingVertical:16
  },
  buttonConfig:{
    width:50,
    height:50,
    backgroundColor:'#d4b626',
    borderRadius: 50,
    marginVertical:10,
    paddingHorizontal:16,
    paddingVertical:16,
    justifyContent:'space-evenly',
},
  buttonText:{
      fontFamily:'Raleway-Medium',
      fontSize:20,
      fontWeight:'500',
      color:'black',
      justifyContent:'center',
      textAlign:'center'

  },
  
})

class Home extends Component {

  static navigationOptions = {
    header:null
  };
  
  render() {
    
    return (
      
        <View style={styles.container}>
            <StatusBar backgroundColor="#ebcc34" barStyle="dark-content" />

        <Text style={[styles.logoText,{paddingTop:50}]}>NEWTON</Text> 
          
            <Image
              style={{width: 250, height: 300, resizeMode:'contain',marginVertical:10}}
              source={require('./atom.png')}
            />
          
            <TouchableOpacity style={[styles.button]} title={'Detalhes'} onPress={() => {this.props.navigation.navigate("Categorias")}}>
                <Text style={styles.buttonText}>Bem-vindo</Text>
            </TouchableOpacity>


            <View style={[styles.viewConfig]}>
                  <TouchableOpacity style={[styles.buttonConfig]} title={'Detalhes'} onPress={() => {this.props.navigation.navigate("Detalhes")}}>
                      <Image
                          style={{width: 35, height: 35, alignSelf:'center'}}
                          source={require('./config.png')}
                      />
                  </TouchableOpacity>
                  
            </View> 

        </View>
      );
  }
}

export default Home;