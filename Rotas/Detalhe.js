import React, {Component} from 'react';

import {
    View,StyleSheet, Keyboard, Alert, TouchableOpacity,Text,TextInput, Image,FlatList,SafeAreaView,StatusBar 

} from "react-native";


import AsyncStorage from '@react-native-community/async-storage';

class Kid {
  constructor(name, gender){
    this.kidName ='nome ' + name;
    this.kidGender = 'sexo ' + gender;

  }
}

  const styles = StyleSheet.create({
      
      list:{
          paddingVertical: 5,
          alignSelf:"center",
          backgroundColor: "#d4b626"
      },
      container :{
          backgroundColor: '#ebcc34',
          flex :1,
          justifyContent:'flex-start',
      },
      print :{
        backgroundColor: '#ebcc34',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
      },

      viewBotoes :{
        backgroundColor: '#ebcc34',
        justifyContent:'space-evenly',
        flexDirection:"row",


      },
      bIcon:{
        width:160,
        height:50,
        backgroundColor:'#d4b626',
        borderRadius: 50,
        marginVertical:10,
        paddingHorizontal:10,
        flexDirection:"row",


      },
      logoText :{
          fontSize:18,
          color : 'white'

      },
      inputBox:{
          fontFamily:'Raleway-Medium',
          width:300,
          backgroundColor:'white',
          borderRadius: 25,
          paddingHorizontal:16,
          marginVertical:10,
          alignSelf:"center",

          
      },

      buttonText:{
          color: 'black',
          paddingHorizontal:10,
          justifyContent:'center',
          alignSelf:"center",
          fontFamily:'PatrickHandSC-Regular',
          fontSize:20,
          fontWeight:'500',
      },
      textStyle:{
          fontFamily:'Raleway-Medium',
          fontSize:25,
          fontWeight:'500',
          flexWrap: 'wrap'
      }
  })

class Detalhe extends Component {
    constructor(props){
        super(props);      
        this.mudouNome = this.mudouNome.bind(this);
        this.state = {nome:"", kids: [],busca:"",};
        this.cadastra=this.cadastra.bind(this);


    }

    gravarTotal = async () => {

        try{
            

            AsyncStorage.setItem('NEWTON_APP', JSON.stringify(this.state.kids));
            console.log("dados salvos"+this.state.kids);
            
            Alert.alert("Sucesso","Dados salvos com sucesso :" +this.state.kids);
            Keyboard.dismiss();
        }catch(e){
            alert(e);
            
        }

    }
    async leia(){
        try{
            let x = await AsyncStorage.getItem('NEWTON_APP');

            if (x!= null){

                console.log("dados carregados:"+x);
                this.setState({kids:  JSON.parse(x)})

                Alert.alert("Sucesso","Dados carregados com sucesso"+ x );
            }
        }catch(e){
            alert(e);
            
        }
    }

    async removeItemValue() {
        try {
            await AsyncStorage.removeItem('NEWTON_APP');
            this.props.navigation.pop();
            return true;
            
        }
        catch(exception) {
            return false;
        }
        
    }

    UNSAFE_componentWillMount(){
       this.leia()
    }


    static navigationOptions = {
      header:null
    };
    
    checaVazio(){
        if(this.state.nome == ''){
            alert('Vazio!')
        }else{
            this.edita(this.state.nome)
        }

    }
    mudouNome(txt){
      this.setState({nome: txt});   
    }


    cadastra(new_kid){
        let vetor = [...this.state.kids];
        const result = vetor.find( obj => obj.kidName === new_kid.kidName );
        if(result){
            alert('Kid Existe!')
        }else{
            this.setState({kids:  [...this.state.kids, new_kid ] });
            console.log(new_kid," adicionada ");
            
            this.props.navigation.pop();
        }
    }

    edita(kid){
      let vetor = [...this.state.kids];
        const result = vetor.find( obj => obj.kidName === kid );
        if(result){
            this.remove(kid)
            this.props.navigation.navigate("Ficha", {funcCadastra: this.cadastra})

        }else{
            alert('Kid nao existe!')
        }
        
    }

    remove(idx){
      let vetor = [...this.state.kids];
      console.log(this.state.kids," kids ");
      const result = vetor.find( obj => obj.kidName === idx );
      if(result){//se encontrou nome
        vetor.splice(result, 1);//remove
        console.log(result," removido ");
        this.setState({kids:  [...vetor] }  );
        console.log(this.state.kids, " kids ");
      }else{
        alert('Usuario nao encontrado!')
      }
    }


    busca(idx){

      let vetor = [...this.state.kids];
      
      const result = vetor.find( obj => obj.kidName === idx );
      if(result){
        while(vetor.length) {
          vetor.pop();
       }
        vetor.push(result);
        console.log(this.state.busca[0], "kid encontrado com sucesso");
        this.setState({busca:  [...vetor] }  );
        console.log(this.state.busca, "vetor busca");
      }else{
        alert('Usuario nao encontrado!')
      }
    }

    renderizaItem(item){

        
        return (
                <View style={styles.print}>
                    <Text style={styles.textStyle}>Questão: {item.kidName}</Text>
                    <Text style={styles.textStyle}>Resposta: {item.kidGender}</Text>
                </View>
                
        )
    }

    render() {

          return (
            <View style={styles.container}>
            <StatusBar backgroundColor="#ebcc34" barStyle="dark-content" />    
                <View style={styles.viewBotoes}>   
                    <TouchableOpacity style={styles.bIcon} onPress={() => {this.props.navigation.navigate("Ficha", {funcCadastra: this.cadastra})}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./add.png')}
                        />
                        
                        <Text style={styles.buttonText}>ADICIONAR QUESTÃO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.remove(this.state.nome)}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./del.png')}
                        />
                        <Text style={styles.buttonText}>DELETA</Text>
                    </TouchableOpacity>
                   
            </View>
            <View style={styles.viewBotoes}>  

                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.busca(this.state.nome)}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./srch.png')}
                        />
                        
                        <Text style={styles.buttonText}>BUSCA</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.checaVazio()}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./edit.png')}
                        />
                        
                        <Text style={styles.buttonText}>EDITA</Text>
                    </TouchableOpacity>


            </View>

            <View style={styles.viewBotoes}>  
                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.gravarTotal()}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./srch.png')}
                        />
                        <Text style={styles.buttonText}>GRAVA</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.bIcon}  onPress={() => {this.removeItemValue()}}>
                        <Image
                            style={{width: 35, height: 35, alignSelf:'center'}}
                            source={require('./srch.png')}
                        />
                        <Text style={styles.buttonText}>DELETAR    TUDO</Text>
                    </TouchableOpacity>
                </View>
                
                    
                <TextInput style={styles.inputBox} placeholder={"Digite um nome"} Type={"String"}  onChangeText={this.mudouNome} />


                <View>
                  <FlatList
                    style={styles.list }
                    data={this.state.busca}
                    renderItem={ ({item}) =>  this.renderizaItem(item)}
                    keyExtractor={({kidBusca}, index) => kidBusca}
                  />
                </View>

                <View>
                    <Text style={styles.buttonText}>{this.state.nome}</Text>
                </View>

                <SafeAreaView>
                    <FlatList
                        style={styles.list}
                        data={this.state.kids}
                        renderItem={ ({item}) =>  this.renderizaItem(item)}
                        keyExtractor={({kidName}) => kidName}
                    />
                </SafeAreaView>

            </View>
          );
    }
}

export default Detalhe;